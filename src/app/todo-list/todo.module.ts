import { NgModule } from '@angular/core';
import { TodoListComponent } from './todo-list.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [TodoListComponent],
  imports: [CommonModule],
  exports: [TodoListComponent]
})
export class TodoModule {}
