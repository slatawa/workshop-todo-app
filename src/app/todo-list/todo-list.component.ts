import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { TodoListState } from '../store/reducers';
import { AddTodoToServer, FetchTodos, ToggleTodo, DeleteTodo } from '../store/actions';
import { Observable } from 'rxjs';
import { Todo } from '../store/model/todo';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  todoListState$: Observable<Todo[]>;

  constructor(private store: Store<TodoListState>) {}

  ngOnInit() {
    this.store.dispatch(new FetchTodos());
    this.todoListState$ = this.store.select(state => state.todos);
  }

  addTodo(event: KeyboardEvent) {
    const todo = {
      id: Math.ceil(Math.random() * 10000),
      description: (<HTMLInputElement>event.target).value,
      isCompleted: false
    };

    this.store.dispatch(new AddTodoToServer(todo));
    this.clearInput();
  }

  toggleTodo(selectedTodo: Todo) {
    this.store.dispatch(new ToggleTodo(selectedTodo));
  }

  deleteTodo(todo: Todo) {
    this.store.dispatch(new DeleteTodo(todo.id));
  }

  clearCompleted() {
    this.todoListState$
      .pipe(take(1))
      .subscribe((todos: Todo[]) =>
        todos.filter(todo => todo.isCompleted).map((todo: Todo) => this.deleteTodo(todo))
      );
  }

  private clearInput() {
    (<HTMLInputElement>event.target).value = '';
  }
}
