import { ActionReducerMap } from '@ngrx/store';
import { todosReducer } from './todosReducer';
import { Todo } from '../model/todo';

export interface TodoListState {
  todos: Todo[];
}

export const reducers: ActionReducerMap<TodoListState> = {
  todos: todosReducer
};
