import { Action } from '@ngrx/store';
import { Todo } from '../model/todo';

export const FETCH_TODOS = 'FETCH_TODOS';
export const ADD_TODO_TO_SERVER = 'ADD_TODO_TO_SERVER';
export const UPDATE_TODOS_STATE = 'UPDATE_TODOS_STATE';
export const TOGGLE_TODO = 'TOGGLE_TODO';
export const DELETE_TODO = 'DELETE_TODO';

export class AddTodoToServer implements Action {
  readonly type = ADD_TODO_TO_SERVER;
  constructor(readonly payload: Todo) {}
}

export class FetchTodos implements Action {
  readonly type = FETCH_TODOS;
}

export class UpdateTodosState implements Action {
  readonly type = UPDATE_TODOS_STATE;
  constructor(readonly payload: Todo[]) {}
}

export class ToggleTodo implements Action {
  readonly type = TOGGLE_TODO;
  constructor(readonly payload: Todo) {}
}

export class DeleteTodo implements Action {
  readonly type = DELETE_TODO;
  constructor(readonly payload: number) {}
}
