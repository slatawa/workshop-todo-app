import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import { Observable, merge } from 'rxjs';
import { switchMap, map, mergeMap } from 'rxjs/operators';

import { ADD_TODO_TO_SERVER, FETCH_TODOS, UpdateTodosState, FetchTodos, DELETE_TODO } from '../actions';
import { Action } from '@ngrx/store';
import { Todo } from '../model/todo';

@Injectable()
export class TodoEffects {
  constructor(private actions$: Actions, private http: HttpClient) {}

  @Effect()
  addTodoToServer$: Observable<any> = this.actions$.pipe(
    ofType(ADD_TODO_TO_SERVER),
    switchMap((action: any) => {
      return this.http
        .post('http://localhost:3000/data', action.payload)
        .pipe(map((todo: Todo) => new FetchTodos()));
    })
  );

  @Effect()
  fetchTodos$: Observable<Action> = this.actions$.pipe(
    ofType(FETCH_TODOS),
    switchMap(() => {
      return this.http
        .get('http://localhost:3000/data')
        .pipe(map((todos: Todo[]) => new UpdateTodosState(todos)));
    })
  );

  @Effect()
  deleteTodo$: Observable<Action> = this.actions$.pipe(
    ofType(DELETE_TODO),
    mergeMap((action: any) => {
      return this.http
        .delete(`http://localhost:3000/data/${action.payload}`)
        .pipe(map(() => new FetchTodos()));
    })
  );
}
